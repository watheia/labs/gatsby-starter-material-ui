FROM node:12 as builder

WORKDIR /home/builder
ADD . .
RUN yarn && yarn build

FROM node:12-alpine

EXPOSE 5000

# Copy resources
COPY --from=builder /home/builder/public /app

CMD ["npx", "serve", "-s", "/app"]
